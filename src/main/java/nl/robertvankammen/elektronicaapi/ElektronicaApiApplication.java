package nl.robertvankammen.elektronicaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElektronicaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElektronicaApiApplication.class, args);
    }
}
