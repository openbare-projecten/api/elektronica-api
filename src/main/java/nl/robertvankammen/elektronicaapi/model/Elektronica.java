package nl.robertvankammen.elektronicaapi.model;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Elektronica {

    @Id
    @GeneratedValue
    private Long id;

    private String soort;

    private int lengte;

    private String locatie;

    @Enumerated
    private Controle controle;

    private String notitie;

    public Long getId() {
        return id;
    }

    public String getSoort() {
        return soort;
    }

    public int getLengte() {
        return lengte;
    }

    public String getLocatie() {
        return locatie;
    }

    public Controle getControle() {
        return controle;
    }

    public String getNotitie() {
        return notitie;
    }
}

enum Controle {
    GOED("Goed"),
    GEDEELTELIJK_WERKEND("Gedeeltelijk werkend"),
    KAPOT("Kapot"),
    ONBEKEND("Onbekend");

    private final String omschrijving;

    Controle(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

}