package nl.robertvankammen.elektronicaapi.repository;

import nl.robertvankammen.elektronicaapi.model.Elektronica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElektronicaRepository extends CrudRepository<Elektronica, Long> {
}
