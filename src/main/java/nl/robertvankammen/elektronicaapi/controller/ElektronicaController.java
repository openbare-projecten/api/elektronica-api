package nl.robertvankammen.elektronicaapi.controller;

import nl.robertvankammen.elektronicaapi.model.Elektronica;
import nl.robertvankammen.elektronicaapi.repository.ElektronicaRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ElektronicaController {

    private final ElektronicaRepository elektronicaRepository;

    public ElektronicaController(ElektronicaRepository elektronicaRepository) {
        this.elektronicaRepository = elektronicaRepository;
    }

    @RequestMapping("/elektronica")
    public Iterable<Elektronica> getAlleElektronica() {
        return elektronicaRepository.findAll();
    }

    @PostMapping("/elektronica")
    public Elektronica nieuweElektrica(@RequestBody Elektronica elektronica) {
        if (elektronica.getId() == null) {
            veldenControle(elektronica);
            return elektronicaRepository.save(elektronica);
        } else {
            throw new IllegalArgumentException("Je mag geen bestaande elektronica in de POST methode sturen, gebruik hiervoor de PUT methode");
        }
    }

    @PutMapping("/elektronica")
    public Elektronica updateElektronica(@RequestBody Elektronica elektronica) {
        Optional<Elektronica> optionalElektronica = elektronicaRepository.findById(elektronica.getId());
        if (optionalElektronica.isPresent()) {
            veldenControle(elektronica);
            return elektronicaRepository.save(elektronica);
        } else {
            throw new IllegalArgumentException("Elektronica met id: " + elektronica.getId() + " bestaat niet, om een nieuwe toetevoegen gebruik de POST methode");
        }
    }

    private void veldenControle(Elektronica elektronica) {
        if (elektronica.getSoort() == null) {
            throw new IllegalArgumentException("elektronica.soort is een verplicht veld");
        }
        if (elektronica.getLocatie() == null) {
            throw new IllegalArgumentException("elektronica.location is een verplicht veld");
        }
        if (elektronica.getControle() == null) {
            throw new IllegalArgumentException("elektronica.controle is een verplicht veld");
        }
    }
}
